import {Directive, Input, HostListener} from '@angular/core';

@Directive({
    selector: '[inputRestrictionPattern]'
})
export class InputRestrictionDirective {
    @Input('inputRestrictionPattern') inputRestrictionPattern: string;

    @HostListener('keypress', ['$event']) onKeyPress(event) {
        if (event.key === '+') {
            event.preventDefault();
        }

        if (event.target.selectionStart > 0 && event.key === '-') {
            event.preventDefault();
        }

        return new RegExp(this.inputRestrictionPattern).test(event.key);
    }
}
